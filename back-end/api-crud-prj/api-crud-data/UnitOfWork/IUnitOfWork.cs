﻿using api_crud_data.Model;
using api_crud_data.Repository;

namespace api_crud_data.UnitOfWork
{
    public interface IUnitOfWork
    {
        IGenericRepository<Employee> EmployeRepository { get; }
        /// <summary>
        /// Commit all changes
        /// </summary>
        int Complete();

        /// <summary>
        /// Commit all changes ASYNC
        /// </summary>
        Task<int> CompleteAsync();

        /// <summary>
        /// Commit all changes ASYNC
        /// </summary>
        Task<int> CompleteAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Discards all changes that has not been commited
        /// </summary>
        void RejectChanges();

        /// <summary>
        /// clean context
        /// </summary>
        void Dispose();

    }


}

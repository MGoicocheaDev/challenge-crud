﻿using api_crud_data.Context;
using api_crud_data.Model;
using api_crud_data.Repository;
using Microsoft.EntityFrameworkCore;

namespace api_crud_data.UnitOfWork
{

    public class UnitOfWork : IUnitOfWork
    {
        private readonly EmployeeContext _context;

        public UnitOfWork(EmployeeContext context)
        {
            _context = context;
        }

        private IGenericRepository<Employee> _employeRepository;
     
        public IGenericRepository<Employee> EmployeRepository
        {
            get { return _employeRepository ?? (_employeRepository = new GenericRepository<Employee>(_context)); }
        }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public async Task<int> CompleteAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public async Task<int> CompleteAsync(CancellationToken cancellationToken)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void RejectChanges()
        {
            foreach (var entry in _context.ChangeTracker.Entries()
                      .Where(e => e.State != EntityState.Unchanged))
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Modified:
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                }
            }

        }
    }


}

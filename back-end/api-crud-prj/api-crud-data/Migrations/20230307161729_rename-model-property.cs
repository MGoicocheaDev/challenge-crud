﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace api_crud_data.Migrations
{
    /// <inheritdoc />
    public partial class renamemodelproperty : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Age",
                table: "Employee");

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Employee",
                type: "TEXT",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "Employee");

            migrationBuilder.AddColumn<int>(
                name: "Age",
                table: "Employee",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);
        }
    }
}

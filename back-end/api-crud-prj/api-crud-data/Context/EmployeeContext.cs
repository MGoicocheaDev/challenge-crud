﻿using api_crud_data.Model;
using Microsoft.EntityFrameworkCore;

namespace api_crud_data.Context
{
    public partial class EmployeeContext : DbContext
    {
        public EmployeeContext()
        {

        }

        public EmployeeContext(DbContextOptions<EmployeeContext> options)
            :base(options)
        {
        }

        public DbSet<Employee> Employes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().ToTable("Employee").Property(x => x.Id);
        }
    }
}

﻿using api_crud_data.Model;

namespace api_crud.Business
{
    public interface IEmployeeBusiness
    {
        Task<IEnumerable<Employee>> GetEmployes();

        Task<Employee> GetEmployeById(long id);

        Task CreateEmploye(Employee employe);

        Task UpdateEmploye(Employee employe);

        Task DeleteEmployeById(long id);


    }
}

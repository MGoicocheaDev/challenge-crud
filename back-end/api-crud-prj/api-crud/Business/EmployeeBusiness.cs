﻿using api_crud.Exceptions;
using api_crud_data.Model;
using api_crud_data.UnitOfWork;

namespace api_crud.Business
{
    public class EmployeeBusiness : IEmployeeBusiness
    {
        private readonly IUnitOfWork _unitOfWork;

        public EmployeeBusiness(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Create Employe
        /// </summary>
        /// <param name="employe">Employe object <see cref="Employee"/></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task CreateEmploye(Employee employe)
        {
            try
            {
                await _unitOfWork.EmployeRepository.AddAsync(employe);
                await _unitOfWork.CompleteAsync();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Delete Employe
        /// </summary>
        /// <param name="id">Identifier from employe</param>
        /// <returns></returns>
        /// <exception cref="NotFoundEmployeeException"></exception>
        public async Task DeleteEmployeById(long id)
        {
            var employe = await _unitOfWork.EmployeRepository.FindByIdAsync(id);

            if (employe == null)
            {
                throw new NotFoundEmployeeException(404, $"The employe with Id {id} doesn´t exists");
            }

            _unitOfWork.EmployeRepository.Remove(employe);
            await _unitOfWork.CompleteAsync();
        }

        /// <summary>
        /// Get Employe by Identifier
        /// </summary>
        /// <param name="id">Identifier from employe</param>
        /// <returns> Employe Object <see cref="Employee"/></returns>
        /// <exception cref="NotFoundEmployeeException"></exception>
        public async Task<Employee> GetEmployeById(long id)
        {
            var employe = await _unitOfWork.EmployeRepository.FindByIdAsync(id);

            if (employe == null)
            {
                throw new NotFoundEmployeeException(404, $"The employe with Id {id} doesn´t exists");
            }
            return employe;
        }

        /// <summary>
        /// Get all employes
        /// </summary>
        /// <returns> List of employes <see cref="Employee"/></returns>
        public async Task<IEnumerable<Employee>> GetEmployes()
        {
            return await _unitOfWork.EmployeRepository.GetAllAsync();
        }

        /// <summary>
        ///     Update specific employe
        /// </summary>
        /// <param name="employe"> object <see cref="Employee"/>  </param>
        /// <returns></returns>
        public async Task UpdateEmploye(Employee employe)
        {
            try
            {
                _unitOfWork.EmployeRepository.Update(employe);
                await _unitOfWork.CompleteAsync();
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}

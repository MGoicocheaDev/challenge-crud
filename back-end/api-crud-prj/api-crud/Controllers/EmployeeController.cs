﻿using api_crud.Business;
using api_crud_data.Model;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace api_crud.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeBusiness _employeBusiness;
        public EmployeeController(IEmployeeBusiness employeBusiness)
        {
            _employeBusiness = employeBusiness;
        }

        // GET: api/<Employe>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var employes = await _employeBusiness.GetEmployes();
            return Ok(employes);
        }

        // GET api/<Employe>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var employe = await _employeBusiness.GetEmployeById(id);
            return Ok(employe); 
        }

        // POST api/<Employe>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Employee value)
        {
            if (!ModelState.IsValid)
            {
                return UnprocessableEntity(ModelState);

            }

            await _employeBusiness.CreateEmploye(value);
            return Ok( new { isSuccess = true, message = "Employe created"});
        }

        // PUT api/<Employe>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] Employee value)
        {
            var employe = await _employeBusiness.GetEmployeById(id);

            employe.FirstName = value.FirstName;
            employe.LastName = value.LastName;
            employe.Email = value.Email;
            employe.Phone = value.Phone;
            employe.Address = value.Address;

            await _employeBusiness.UpdateEmploye(employe);

            return Ok(new { isSuccess = true, message = "Employe Updated" });
        }

        // DELETE api/<Employe>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {

            await _employeBusiness.DeleteEmployeById(id);
            return Ok(new { isSuccess = true, message = "Employe Deleted" });
        }
    }
}

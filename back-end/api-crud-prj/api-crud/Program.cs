using api_crud.Business;
using api_crud.Exceptions;
using api_crud_data.Context;
using api_crud_data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;

var builder = WebApplication.CreateBuilder(args);

//Get connectionString 
var connectionString = builder.Configuration.GetConnectionString("employeDB") ?? throw new InvalidOperationException("Connection string 'employeDB' not found.");

builder.Services.AddDbContext<EmployeeContext>(options =>
    options.UseSqlite(connectionString));
// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddTransient<IEmployeeBusiness, EmployeeBusiness>();

///Unit of work dependency injection
builder.Services.AddTransient<IUnitOfWork, UnitOfWork>();

//services cors
builder.Services.AddCors(p => p.AddPolicy("corsapp", builder =>
{
    builder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
}));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseCors("corsapp");
app.UseMiddleware<ErrorHandlerMiddleware>();
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

//Create Db if not exist
using var scope = app.Services.CreateScope();
await using var dbContext = scope.ServiceProvider.GetRequiredService<EmployeeContext>();
await dbContext.Database.MigrateAsync();

app.Run();

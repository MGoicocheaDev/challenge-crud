﻿using System.Runtime.Serialization;

namespace api_crud.Exceptions
{
    [Serializable()]
    public class NotFoundEmployeeException : Exception, ISerializable
    {
        public int statusCode { get; set; }
        public string statusDescription { get; set; }

        public NotFoundEmployeeException(int statusCode, string statusDescription)
            : base($"{statusDescription}")
        {
            this.statusCode = statusCode;
            this.statusDescription = statusDescription;
        }
    }
}

# Prueba Técnica
Desarrollar una aplicación de mantenimiento de empleado con 5 campos mínimo .net Core y Front End Angular.

- Back: Net 6
- Front: Angular
- Database: Sqlite

## Estructura de proyecto
- BackEnd
    - api-crud
- FrontEnd
    - crud-app
- docker-compose.yml

## Ejecución

El proyecto se encuentra en contenedores de docker para un facil despliegue.
Para ejecutar la aplicación se debe ejcutar el siguiente comando desde la raiz del directorio de proyecto.

```
> docker-compose up
```

Luego de ejecutar, docker empezara la compilación y despliegue de la aplicación.
Se visualizara una ventana de comandos similar a la siguiente:
![alt text](images/comsole-docker-compose-up.png "Console docker compose up")

Se podra validar la ejecución de los contenedores con el siguiente comando:

```
> docker ps -a
```
![alt text](images/docker-ps.png "Console docker compose up")

Confirmado la ejecución de los contenedores:
 -  crud-app
 - api-crud

 Se podra iniciar la aplicación, ingresando al siguiente enlace desde el navegador.

 - Front: http://localhost
 - Back: http://localhost:8082
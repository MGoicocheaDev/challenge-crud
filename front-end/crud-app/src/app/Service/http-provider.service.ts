import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WebApiService } from './web-api.service';
import { environment } from 'src/environments/environment';

var apiUrl = environment.apiUrl;

var httpLink = {
  baseUrl: apiUrl + "api/Employee",
}

@Injectable({
  providedIn: 'root'
})
export class HttpProviderService {

  constructor(private webApiService: WebApiService) { }

  public getAllEmployee(): Observable<any> {
    return this.webApiService.get(httpLink.baseUrl);
  }

  public getEmployee(id: any): Observable<any> {
    return this.webApiService.get(httpLink.baseUrl + '/'+ id);
  }

  public createEmployee(model: any): Observable<any> {
    return this.webApiService.post(httpLink.baseUrl, model);
  }  

  public deleteEmployee(id: any): Observable<any> {
    return this.webApiService.delete(httpLink.baseUrl + '/'+ id);
  } 

  public updateEmployee(id: any,model: any): Observable<any> {
    return this.webApiService.put(httpLink.baseUrl + '/'+ id, model);
  } 
}
